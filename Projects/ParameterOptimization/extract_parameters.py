"""extract_parameters.py - extract EMT2013 parameters from optimization, write as dictionary.

Usage:

python extract_parameters [options] directory [directory ...] output.py

where the directories are one or more directory containing fit.dat and fit-*.dat files,
output.py is the human- and Pythonreadable output file with the extracted parameters,
and options is one of

--same:  All directories XXXX
"""



import os
import sys
import pprint
import numpy as np

#from asap3.Tools.MaterialProperties import MaterialPropertiesData
#from asap3.Tools.ParameterOptimization import ParameterPerformance
#from asap3.Tools.ParameterOptimization.Optimization import ParameterOptimization
#from asap3.Tools.ParameterOptimization.SearchParallel import ParameterSearch
#from asap3.Tools.ParameterOptimization.EMT import EMT2011Fit, EMTStdParameters
from asap3.Tools.ParameterOptimization.GetParameters import get_parameters
#from asap3 import EMT


if len(sys.argv) < 3:
    print >>sys.stderr, "ERROR: Too few arguments.\n\n\n"
    print >>sys.stderr, __doc__
    sys.exit(1)

output = sys.argv[-1]
indirs = sys.argv[1:-1]
print "Number of input folders:", len(indirs)

parameters = {}
errorfunctions = {}


for dir in indirs:
    print "Processing", dir
    for line in open(os.path.join(dir, 'fit.dat')):
        words = line.split()
        if words and words[0] == "0":
            paramfile = os.path.join(dir, "fit-"+words[1]+".dat")
            errfunc = float(words[2])
            break
    print "  Reading {0} (error function = {1:.1f})".format(paramfile, errfunc)
    param, errf = get_parameters(paramfile, 2)
    assert(abs(errfunc - errf) < 0.1)
    # Convert keys from (symb, symb) to symb.
    for k, v in param.items():
        k0, k1 = k
        assert(k0 == k1)
        if k0 in parameters:
            # Already present, replace if better
            if errfunc < errorfunctions[k0]:
                print "    Using improved parameters for", k0
                parameters[k0] = v
                errorfunctions[k0] = errfunc
        else:
            print "    Storing parameters for", k0
            parameters[k0] = v
            errorfunctions[k0] = errfunc

print "Converting parameters from tuples to dictionaries"
parameternames = ['eta2', 'lambda', 'kappa', 'E0', 'V0', 'S0', 'n0']
for k, v in parameters.items():
    parameters[k] = dict(zip(parameternames, v))

print "Writing parameters to", output
out = open(output, "w")
pprint.pprint(parameters, stream=out)
out.close()
