import pickle
import sys
import os
f = open(sys.argv[1])
#File dir with amc.gz files
dr = sys.argv[2]
if dr[-1]=="/":
	del dr[-1] #Delete last char of string
f.read(len("parametermontecarlo"))
p = pickle.load(f)
a = pickle.load(f)
print "Length of counts:" + str(len(a['counts']))
print "Sum of counts:" + str( sum(a['counts']) )
print a['counts']
#find the minimum number of counts and the number of configurations with this multiplicity
mn = min(a['counts'])
ma= [i for i in a['counts'] if i==mn]
mp = len(ma)
print "Minimum number of counts: "+str(mn)
print "Number of times this min. occours: "+str(mp)

#Now find the missing files in the dir:
missing = []
for n in range(len(a['counts'])):
	if not os.path.exists(dr+"/"+"a%05i.amc.gz" % n):
		missing.append("a%05i.amc.gz" % n)
print "Missing "+str(len(missing))+" amc.gz files in sim_dir"


f.close()
