import matplotlib.pyplot as plt
import cPickle as pickle
import numpy as np
import sys
import gzip
from asap3.nanoparticle_mc.atommontecarlodata import *
fn = sys.argv[1]#Filename
p = AtomMonteCarloData()

p.read(fn)


E_mc = np.array(p.lists['energy'])
E_urel = np.array(p.lists['unrelaxed_energies'])
E_rel = np.array(p.lists['relaxed_energies'])

E = E_mc + E_rel - E_urel
I = range(0,len(E_mc))
print len(E_mc)
plt.figure(1)

plt.subplot(211)
titstr = fn.split('_')[-1].split('.')[0]
plt.title(titstr)
plt.plot(I,E[I], 'k',label="Ereal")
plt.legend( loc=2, borderaxespad=0.)



plt.subplot(212)

plt.plot(I,E_mc[I],'k',label = "Emc")
plt.legend( loc=2, borderaxespad=0.)


plt.show()

#now plot the first few:

#plt.plot(E_mc[range(0,len(E_mc),1)])
#plt.show()
