#PBS -l nodes=1:ppn=4:opteron4
#PBS -q verylong
#PBS -N amc_Tfict400_n100_end
#PBS -m ae
#ASAP S
"""Ends atom simulation and gives result in file.
Usage:
       endatomsimulation.py amcfolder  
"""
import sys
import os
from asap3.nanoparticle_mc.atommontecarlodata import AtomMonteCarloData
import numpy as np
from ase import units #used to get kB in eV/K
from mc_result import *
#Check if user asked for help with -h or --help or -doc
for a in sys.argv:
    if a=="-h" or a == "--help" or a == "-doc" or a == "--documentation":
        print >> sys.stderr, __doc__
        sys.exit(0)

fdir = sys.argv[1] #Directory of .amc files to join

out = sys.stdout
print >> out, "Atom Monte Carlo Simulation Summary \n -------------------------------------------"



files = []
os.chdir(fdir)

for f in os.listdir("./"):
    if f.endswith(".amc.gz"):
        files.append(f)
#Now we know the file names. Read merge the informations.
dtemp = AtomMonteCarloData()
#Init arrays
naccept = []

files = [files[n] for n in np.random.choice(len(files), size=200, replace=False)]
#step = 1
step = len(files) // 20
next_print = 0
n = 0

for f in files: #Now cut half the MC steps off, to only consider Thermal equilibrium.
    n += 1
    if n > next_print:
        print n, "of", len(files)
        next_print += step
    #At the same time find the global min. energy
    try:
        dtemp.read(f) #loads pickle to dtemp!
    except:
        sys.stderr.write("Error in file"+str(f))
        
    #ninsane += dtemp.ninsane
    naccept.append(len(dtemp.lists['energy']) - dtemp.id_relax)

avg = sum(naccept) / len(naccept)
print "n_accept min: {0}   max: {1}   avg: {2}".format(min(naccept), max(naccept), avg)
