from asap3 import *
from asap3.constraints import FixAtoms
from ase.lattice.cubic import FaceCenteredCubic
from ase.io import read, write
from asap3.io import PickleTrajectory
from asap3.io import BundleTrajectory
from ase.io import Trajectory

fn = "testconstraint"
atoms = FaceCenteredCubic(symbol='Cu', size=(3,3,3))
c = FixAtoms(indices=(2,3))
atoms.set_constraint(c)

traj = PickleTrajectory(fn+'.trj', "w", atoms, _warn=False)
traj.write()
traj.close()
del traj

traj = BundleTrajectory(fn+'.bundle', "w", atoms)
traj.write()
traj.close()
del traj

traj = Trajectory(fn+'.traj', "w", atoms)
traj.write()
traj.close()
del traj

traj2 = PickleTrajectory(fn+'.trj', _warn=False)
atoms2 = traj2[-1]
c2 = atoms2.constraints[0]

print "*** PickleTrajectory"
print "Original class:", c.__class__
print "New class:", c2.__class__
assert c.__class__ is c2.__class__

traj2 = read(fn+'.bundle')
c2 = atoms2.constraints[0]

print "*** BundleTrajectory"
print "Original class:", c.__class__
print "New class:", c2.__class__
assert c.__class__ is c2.__class__

print "Test passed."

