from numpy import *
from asap3 import *

P = 10.55
Q = 2.43
A = 4.3724/23.0655
xi = 29.041/23.0655
r0 = 3.615/sqrt(2.)
q = [1.8*18.22283/sqrt(23.0655), -1.2*18.22283/sqrt(23.0655)]
kappa = .08
D = [0,21.6893/23.0655,0]
alpha = [0,2,0]
R0 = [0,1.8,0]
a = [1.161, 1.8883]
b = [.1101, .1802]
f0 = .0433641
E=[-6.4/23.0655,-4.8/23.0655,-3.05/23.0655]
rho0=[2.6,2.1,2.2]
l0=[.48,.575,.375]
B=[0,0,0]
C=[0,0,0]

## The following calculations should give the same result without the bug

## Calculation using periodic atoms (correct result)
cutOff = 1.01 # cutOff

calc=MetalOxideInterface2(
    P, Q, A, xi, r0, cutOff,
    q, kappa, D, alpha, R0, a, b, f0, cutOff, 
    E, rho0, l0, B, C, cutOff)

dr = 1. # Distance between interacting atoms
L = 10. 

atoms = Atoms('AlO',[(0,0,0),(0,0,L)],pbc=(0,0,1))
atoms.set_cell([[1,0,0],[0,1,0],[0,0,L+dr]]) # Cell size larger than 2.05*cutOff

atoms.set_calculator(calc)

assign = ones(len(atoms),dtype=int32)
assign[atoms.get_atomic_numbers() == 29] = 0
assign[atoms.get_atomic_numbers() == 8] = 2
atoms.new_array('assignment', int32(assign))
monolayer = zeros(len(atoms),dtype=int32)
atoms.new_array('monolayer', int32(monolayer))

print 'Periodic atoms: ', atoms.get_potential_energy()

##########################################################################

# Calculation using image atoms (wrong result)
cutOff = 1.01 # cutOff

calc=MetalOxideInterface2(
    P, Q, A, xi, r0, cutOff,
    q, kappa, D, alpha, R0, a, b, f0, cutOff, 
    E, rho0, l0, B, C, cutOff)

dr = 1. # Distance between interacting atoms
L = 1.05 # Still larger than cutOff i.e. no interaction inside the cell

atoms = Atoms('AlO',[(0,0,0),(0,0,L)],pbc=(0,0,1))
atoms.set_cell([[1,0,0],[0,1,0],[0,0,L+dr]]) # Cell size smaller than 2.05*cutOff

atoms.set_calculator(calc)

assign = ones(len(atoms),dtype=int32)
assign[atoms.get_atomic_numbers() == 29] = 0
assign[atoms.get_atomic_numbers() == 8] = 2
atoms.new_array('assignment', int32(assign))
monolayer = zeros(len(atoms),dtype=int32)
atoms.new_array('monolayer', int32(monolayer))

print 'Image atoms: ', atoms.get_potential_energy()
